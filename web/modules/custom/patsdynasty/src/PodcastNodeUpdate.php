<?php

namespace Drupal\patsdynasty;


use Drupal\node\Entity\Node;

class PodcastNodeUpdate {
  public static function updateNode($node, $episode, $provider, &$context) {
    $results = [];
    $node = Node::load($node->id());
    // Get the episode start date and figure out downloads for first 30 days.
    $start = $node->get('created')->value;
    // Find $start +30 days.
    $first30 = $start + (60 * 60 * 24 * 30);
    // Set provider & total download numbers.
    if ($provider == 'acast') {
      $node->field_acast_downloads->value = $episode['totals']['acast'];
      $spotify_total = $node->get('field_spotify_downloads')->value;
      $node->field_total_downloads-> value = $episode['totals']['acast'] + $spotify_total;
      $acast30 = 0;
      foreach ($episode['daily'] as $time => $dls) {
        if ($time <= $first30) {
          $acast30 += $dls;
        }
      }
      $node->field_first_30_acast->value = $acast30;
      $node->field_first_30_days->value = $acast30 + $node->get('field_first_30_spotify')->value;
    } elseif ($provider == 'spotify') {
      $node->field_spotify_downloads->value = $episode['totals']['spotify'];
      $acast_total = $node->get('field_acast_downloads')->value;
      $node->field_total_downloads-> value = $episode['totals']['acast'] + $acast_total;
      $spotify30 = 0;
      foreach ($episode['daily'] as $time => $dls) {
        if ($time <= $first30) {
          $spotify30 += $dls;
        }
      }
      $node->field_first_30_spotify->value = $spotify30;
      $node->field_first_30_days->value = $node->get('field_first_30_acast')->value + $spotify30;
    }

    $results[] = $node->save();
    $context['results'] = $results;
  }
}
