<?php
/**
 * @file
 * Contains Drupal\patsdynasty\Form\PodcastDownloadForm.
 */
namespace Drupal\patsdynasty\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;

class PodcastDownloadForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'patsdynasty.podcastdownload',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'podcastdownload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['podcast_csv'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Analytics CSV file'),
      '#upload_location' => 'public://csv',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
    ];
    $form['provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Analytics provider'),
      '#options' => [
        'acast' => $this
          ->t('Acast'),
        'spotify' => $this
          ->t('Spotify'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $form_file = $form_state->getValue('podcast_csv', 0);
    // Save the CSV file.
    if (isset($form_file[0]) && !empty($form_file[0])) {
      $file = File::load($form_file[0]);
      $file->setPermanent();
      $file->save();

      $provider = $form_state->getValue('provider');

      // Get podcasts in the system.
      $nids = \Drupal::entityQuery('node')->condition('type','podcast_episode')->execute();
      $pod_nodes = Node::loadMultiple($nids);
      // Get a list of titles.
      $current_episodes = $this->get_episode_titles($pod_nodes);

      // Parse the CSV file and extract the data.
      $episode_data = $this->parse_csv($file->url(),  $provider);


      // Save the data to each podcast node.
      $operations = [];
      foreach ($episode_data as $episode) {
        // Get the current podcast node from $pod_nodes.
        $nid = $current_episodes[$episode['title']];
        $node = $pod_nodes[$nid];
        if ($node) {
          $operations[] = ['\Drupal\patsdynasty\PodcastNodeUpdate::updateNode', [$node, $episode, $provider]];
        }
      }
      $batch = [
        'title' => 'Updating Podcast Episode nodes',
        'operations' => $operations,
        'progress_message' => 'Processed @current out of @total.',
      ];
      batch_set($batch);
    }
  }

  private function parse_csv($uri, $provider) {
    $row = 1;
    $episodes = [];
    if (($handle = fopen($uri, "r")) !== FALSE) {
      // Go through each row of the CSV.
      while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $num = count($data);
        // The first row has the header information; use it to build the initial
        // array of episodes.
        if ($row == 1) {
          for ($c=0; $c < $num; $c++) {
            if ($c > 0 && !isset($episodes[$c])) {
              $episodes[$c] = [
                'title' => $data[$c],
                'daily' => [],
                'monthly' => [
                  '2020' => []
                ],
                'totals' => [
                  'acast' => 0,
                  'spotify' => 0
                ]
              ];
            }
          }
        }
        else {
          // Go through the rest of the rows and match the download column to
          // the correct episode array item.
          foreach($episodes as $c => $ep) {
            $date = $data[0];
            $timestamp = strtotime($date);
            $month = date('F', $timestamp);
            $year = date('Y', $timestamp);
            // Only add entries after Feb 1st, 2020.
            if ($timestamp > 1580619600) {
              $downloads = $data[$c];
              if (!isset($episodes[$c][$timestamp])) {
                $episodes[$c]['daily'][$timestamp] = $downloads;
              }
              else {
                $episodes[$c]['daily'][$timestamp] += $downloads;
              }
              if ($downloads != null) {
                $episodes[$c]['totals'][$provider] += $downloads;
              }
              if (!isset($episodes[$c]['monthly'][$year][$month])) {
                $episodes[$c]['monthly'][$year][$month] = $downloads;
              }
              else {
                $episodes[$c]['monthly'][$year][$month] += $downloads;
              }
            }
          }
        }
        $row++;

      }
      fclose($handle);
    }
    return $episodes;
  }

  /**
   * Get the episode titles from list of nodes.
   */
  private function get_episode_titles($pod_nodes) {
    $titles = [];

    foreach ($pod_nodes as $pod) {
      $titles[$pod->label()] = $pod->id();
    }
    return $titles;
  }


}
