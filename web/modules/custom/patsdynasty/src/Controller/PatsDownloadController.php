<?php

namespace Drupal\patsdynasty\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;

class PatsDownloadController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    $episodes = [
      'totals' => [],
      'last30' => 0
    ];
    // Get podcasts in the system.
    $nids = \Drupal::entityQuery('node')->condition('type','podcast_episode')->execute();
    $pod_nodes = Node::loadMultiple($nids);
    $totals = [];
    // Get a list of titles.
    $current_episodes = $this->get_episode_titles($pod_nodes);
    // Get info from CSV.
    $csvs = ['acast', 'spotify'];
    foreach ($csvs as $p) {
      $episodes = $this->csv_analytics($episodes, $p, $pod_nodes);
    }
    // Set totals and last 30 days counts in their own variables.
    $totals = $episodes['totals'];
    $last30 = $episodes['last30'];
    unset($episodes['totals']);
    unset($episodes['last30']);
    // Remove all the other junk that is in here for some reason.
    foreach ($totals as $ep => $item) {
      if ($item == null) {
        unset($totals[$ep]);
      }
    }
    // Remove any episodes that are in analytics but not published yet.
    foreach ($episodes as $key => $ep) {
      if (!in_array($ep['title'], $current_episodes)) {
        unset($episodes[$key]);
      }
    }

    $top_ten = $this->top_ten($totals);
    // Create display array.
    $build = [
      '#theme' => 'pats_pod_analytics',
      '#episodes' => $episodes,
      '#totals' => $top_ten,
      '#last30' => $last30
    ];
    return $build;
  }

  public function csv_analytics($episodes, $provider, $nodes) {
    $row = 1;
    $uri = DRUPAL_ROOT . '/modules/custom/patsdynasty/analytics/'.$provider.'_analytics.csv';
    if (($handle = fopen($uri, "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $num = count($data);
        if ($row == 1) {
          for ($c=0; $c < $num; $c++) {
            if ($c > 0 && !isset($episodes[$c])) {
              $episodes[$c] = [
                'title' => $data[$c]
              ];
            }
          }
        }
        else {
          foreach($episodes as $c => $ep) {
            $date = $data[0];
            $timestamp = strtotime($date);
            $last30 = date('U', strtotime('-30 days'));
            // Only add entries after Feb 1st, 2020.
            if ($timestamp > 1580619600) {
              $downloads = $data[$c];
              $title = $episodes[$c]['title'];
              if (!isset($episodes[$c][$timestamp])) {
                $episodes[$c][$timestamp] = $downloads;
              }
              else {
                $episodes[$c][$timestamp] += $downloads;
              }
              if ($downloads != null) {
                $episodes['totals'][$title][$provider] += $downloads;
              }
              if ($timestamp > $last30) {
                $episodes['last30'] += $downloads;
              }
            }
          }
        }
        $row++;

      }
      fclose($handle);
    }
    return $episodes;
  }

  function top_ten($totals) {
    $top_ten = [];
    $return = [];
    foreach ($totals as $ep => $numbers) {
      $t = $numbers['acast'] + $numbers['spotify'];
      $top_ten[$ep] = $t;
    }
    arsort($top_ten);
    foreach ($top_ten as $title => $count) {
      $return[$title] = $totals[$title];
    }
    return $return;
  }

  function get_episode_titles($pod_nodes) {
    $titles = [];

    foreach ($pod_nodes as $pod) {
      $titles[] = $pod->label();
    }
    return $titles;
  }
}
