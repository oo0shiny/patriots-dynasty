<?php

namespace Drupal\patsdynasty\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\JsonResponse;
use GuzzleHttp\Client;
use \Drupal\node\Entity\node;

class PatsdynastyController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    $brady = $this->brady_stats();

    $header = array(
      array('data' => t('QB')),
      array('data' => t('Att')),
      array('data' => t('Comp')),
      array('data' => t('Comp %')),
      array('data' => t('INT')),
      array('data' => t('TDs')),
      array('data' => t('Yards')),
      array('data' => t('Comebacks')),
      array('data' => t('GW Drives')),
    );

    $rows[] = array(
      'class' => ['red-text'],
      'data' => [
        'qb' => 'Tom Brady',
        'att' => $brady['att'],
        'comp' => $brady['comp'],
        'comp_pct' => $brady['comp_pct'],
        'int' => $brady['int'],
        'tds' => $brady['tds'],
        'yds' => $brady['yds'],
        'q4c' => $brady['q4c'],
        'gwd' => $brady['gwd'],
      ]
    );

    // Get all QB nodes.
    $nids = \Drupal::entityQuery('node')->condition('type','quarterback')->execute();
    $qb_nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);

    foreach ($qb_nodes as $qb) {
      $nid = $qb->id();
      $qb_link = Link::createFromRoute($qb->getTitle(), 'entity.node.canonical', array('node' => $nid));
      $att = $qb->get('field_reg_att')->value + $qb->get('field_poff_att')->value;
      $comp = $qb->get('field_reg_comp')->value + $qb->get('field_poff_comp')->value;
      $comp_pct = ($qb->get('field_reg_comp_pct')->value + $qb->get('field_poff_comp_pct')->value) / 2;
      $int = $qb->get('field_reg_pass_ints')->value + $qb->get('field_poff_pass_ints')->value;
      $tds = $qb->get('field_reg_pass_tds')->value + $qb->get('field_poff_pass_tds')->value;
      $yds = $qb->get('field_reg_pass_yards')->value + $qb->get('field_poff_pass_yds')->value;
      $q4c = $qb->get('field_reg_comebacks')->value + $qb->get('field_poff_comebacks')->value;
      $gwd = $qb->get('field_reg_game_winning_drives')->value + $qb->get('field_poff_game_winning_drives')->value;

      $rows[] = array(
        'qb' => $qb_link,
        'att' => $att,
        'comp' => $comp,
        'comp_pct' => $comp_pct,
        'int' => $int,
        'tds' => $tds,
        'yds' => $yds,
        'q4c' => $q4c,
        'gwd' => $gwd,
      );
    }
    $build['qb_table'] = array(
      '#theme' => 'table',
      '#attributes' => array('id' => 'qb-table'),
      '#header' => $header,
      '#rows' => $rows
    );
    return $build;
  }

  private function brady_stats() {
    $brady = array();

    $brady['gwd'] = 61;
    $brady['q4c'] = 47;

    $connection = \Drupal::database();
    // Get Brady's TDs.
    $query = $connection->query('SELECT SUM(td.field_brady_tds_value) FROM node__field_brady_tds td');
    $brady['tds'] = $query->fetchField();

    // Get Brady's INTs.
    $query = $connection->query('SELECT SUM(ints.field_brady_ints_value) FROM node__field_brady_ints ints');
    $brady['int'] = $query->fetchField();

    // Get Brady's Yards.
    $query = $connection->query('SELECT SUM(yd.field_brady_yards_value) FROM node__field_brady_yards yd');
    $brady['yds'] = $query->fetchField();

    // Get Brady's Attempts
    $query = $connection->query('SELECT SUM(att.field_brady_attempts_value) FROM node__field_brady_attempts att');
    $brady['att'] = $query->fetchField();

    // Get Brady's Completions
    $query = $connection->query('SELECT SUM(comp.field_brady_completions_value) FROM node__field_brady_completions comp');
    $brady['comp'] = $query->fetchField();



    // Calculations:
    // Completion Percent.
    $brady['comp_pct'] = round(($brady['comp'] / $brady['att']) * 100, 1);
    // TD Percent.
    $brady['td_pct'] = round(($brady['tds'] / $brady['att']) * 100, 1);
    // Yards/Attempt.
    $brady['ypa'] = round(($brady['yds'] / $brady['att']), 1);
    // Yards/Completion.
    $brady['ypc'] = round(($brady['yds'] / $brady['comp']), 1);
    // QB Rating.
    $brady['qbr'] = $this->qb_rating($brady['comp'], $brady['att'], $brady['yds'], $brady['tds'], $brady['int']);

    return $brady;
  }

  private function qb_rating($comp, $att, $yds, $td, $int) {
    $rating = 0;
    $a = (($comp/$att) - 0.3) * 5;
    $b = (($yds/$att) - 3) * .25;
    $c = ($td/$att) * 20;
    $d = 2.375 - (($int/$att) * 25);

    $rating = (($a + $b + $c + $d) / 6) * 100;

    if ($rating > 0) {
      return round($rating, 1);
    }
    else {
      return $rating = 0;
    }
  }
  /**
   * 1. Accepts arguments from a Node.js listener
   * 2. Parses arguments and builds a query of highlights.
   * 3. Returns json response with list of highlight gifs.
   * @param $arguments
   * @return JSON response
   */
  public function patsbot($arguments) {
    $search = [
      'base' => 'https://patriotsdynasty.info/patsbot?',
      'year' => 'year=',
      'week' => 'week=',
      'team' => 'opponent=',
      'player' => ''
    ];
    $teams = [
      'cardinals' => '1',
      'falcons' => '13',
      'ravens' => '25',
      'bills' => '18',
      'panthers' => '16',
      'bears' => '12',
      'bengals' => '26',
      'browns' => '27',
      'cowboys' => '2',
      'broncos' => '22',
      'lions' => '10',
      'packers' => '9',
      'texans' => '28',
      'colts' => '30',
      'jaguars' => '31',
      'chiefs' => '20',
      'chargers' => '23',
      'rams' => '7',
      'dolphins' => '17',
      'vikings' => '11',
      'saints' => '15',
      'giants' => '3',
      'jets' => '19',
      'raiders' => '21',
      'eagles' => '5',
      'steelers' => '24',
      '49ers' => '8',
      'seahawks' => '6',
      'buccaneers' => '14',
      'bucs' => '14',
      'titans' => '29',
      'redskins' => '4'
    ];
    $add_to_search = [];
    $player_count = 0;

    $args_array = explode(' ', $arguments);
    foreach ($args_array as $arg) {
      if ($arg != '!patsbot' && $arg != 'week' && $arg != '') {
        // Check if argument is a number (week/season).
        if (is_numeric($arg)) {
          // If number is bigger than 100 it's probably a year.
          if ($arg > 100) {
            $search['year'] .= $arg;
            $add_to_search[] = 'year';
          }
          // Otherwise it's probably a week.
          else {
            $search['week'] .= $arg;
            $add_to_search[] = 'week';
          }
        }
        // Argument is a string, check if team or player.
        else {
          $arg = strtolower($arg);
          // Check if it's a team.
          if (array_key_exists($arg, $teams)) {
            $search['team'] .= $teams[$arg];
            $add_to_search[] = 'team';
          }
          // Last check is if it's a player.
          else {
            if ($arg == 'goat') {
              $arg = 'tom-brady';
            }
            // Check if it's a firstname_lastname situation.
            if (strpos($arg, '-') !== FALSE) {
              $arg = str_replace('-', '%20', $arg);
            }
            $player_api = 'http://patriotsdynasty.info/patsplayers?name=' . $arg;
            $client = new Client();
            $res = $client->get($player_api);
            $json = json_decode($res->getBody());
            foreach ($json as $player) {
              if($player_count < 1) {
                $search['player'] .= 'player=' . $player->id;
                $add_to_search[] = 'player';
              }
              else {
                $search['player'] .= '&player=' . $player->id;
              }
              $player_count++;
            }
          }
        }
      }
    }
    // Build the query string to get gifs.
    $gif_api = $search['base'];
    foreach ($add_to_search as $arg) {
      $add = $search[$arg] . '&';
      $gif_api .= $add;
    }
    $gif_search = new Client();
    $result = $gif_search->get($gif_api);
    $gifs = json_decode($result->getBody(), true);
    return new JsonResponse($gifs);
  }

  function display_record() {
    $filter = \Drupal::request()->request->get('filter');
    $content = [];
    if ($filter == 'month' || $_GET['filter'] == 'month') {
      // Get monthly win pct for all games.
      $team_nids = $this->get_games();
      $games = $this->get_game_data($team_nids);
      foreach ($games as $month => $data) {
        $content['team'][$month] = [
          'w' => $data['Win'],
          'l' => $data['Loss'],
          'pct' => ($data['Win'] / ($data['Win'] + $data['Loss']))
        ];
      }
      $brady_nids = $this->get_games(true);
      $brady_games = $this->get_game_data($brady_nids);
      foreach ($brady_games as $month => $data) {
        $content['brady'][$month] = [
          'w' => $data['Win'],
          'l' => $data['Loss'],
          'pct' => ($data['Win'] / ($data['Win'] + $data['Loss']))
        ];
      }
    }
    return [
      '#theme' => 'record_by_month',
      '#graph' => $content,
    ];
  }

  private function get_games($brady = NULL){
    $game_nids = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'game');
    if (!is_null($brady)) {
      $game_nids->condition('field_did_brady_play_', true);
    }
    return $game_nids->execute();
  }

  private function get_game_data($nids) {
    $standings = [];
    foreach (Node::loadMultiple($nids) as $game) {
      $date  = $game->get('field_date')->value;
      $month = substr($date, 5, 2);
      $result = $game->get('field_result')->value;
      if (isset($standings[$month][$result])) {
        $standings[$month][$result]++;
      }
      else {
        $standings[$month][$result] = 1;
      }
    }
    return $standings;
  }
}
