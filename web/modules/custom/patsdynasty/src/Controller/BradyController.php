<?php

namespace Drupal\patsdynasty\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\JsonResponse;
use GuzzleHttp\Client;
use \Drupal\node\Entity\node;

class BradyController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {

    return [
      '#theme' => 'brady_viz',
    ];
  }
}
