<?php
/**
 * @file
 * Contains \Drupal\dicefield\Plugin\Field\FieldWidget\DiceWidget.
 */

namespace Drupal\patsdynasty\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'dice' widget.
 *
 * @FieldWidget (
 *   id = "poddownload",
 *   label = @Translation("Podcast Download widget"),
 *   field_types = {
 *     "poddownload"
 *   }
 * )
 */
class PodDownloadWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element['date'] = array(
      '#type' => 'date',
      '#title' => t('Episode date'),
      '#default_value' => time(),
    );
    $element['downloads'] = array(
      '#type' => 'number',
      '#title' => t('Episode Downloads'),
      '#default_value' => 0,
    );

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('container-inline')),
      );
    }

    return $element;
  }
}
