<?php

/**
 * @file
 * Contains \Drupal\dicefield\Plugin\Field\FieldType\Dice.
 */

namespace Drupal\patsdynasty\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'poddownload' field type.
 *
 * @FieldType (
 *   id = "poddownload",
 *   label = @Translation("Podcast Download"),
 *   description = @Translation("Stores the date and number of downloads for a podcast episode."),
 *   default_widget = "poddownload",
 *   default_formatter = "poddownload"
 * )
 */
class PodDownload extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'date' => array(
          'type' => 'int',
        ),
        'downloads' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value1 = $this->get('date')->getValue();
    $value2 = $this->get('downloads')->getValue();
    return empty($value1) && empty($value2) && empty($value3);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['date'] = DataDefinition::create('timestamp')
      ->setLabel(t('Date'))
      ->setDescription(t('Timestamp of episode date'));

    $properties['downloads'] = DataDefinition::create('integer')
      ->setLabel(t('Downloads'))
      ->setDescription(t('The number of downloads for this date'));

    return $properties;
  }
}
