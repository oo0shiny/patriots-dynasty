<?php

namespace Drupal\patsdynasty\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
/**
 * Provides a Block that displays games from this date in history.
 *
 * @Block(
 *   id = "on_this_day",
 *   admin_label = @Translation("On this day"),
 *   category = @Translation("Custom"),
 * )
 */
class OnThisDay extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get all games and see if the day/month matches up to today.
    $today = date('m-d');
    $game_nids = \Drupal::entityQuery('node')
    ->condition('type', 'game')
    ->execute();

    $games = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($game_nids);

    $build = [
      '#theme' => 'on_this_day',
      '#games' => []
    ];
    foreach ($games as $game) {
      $game_date = substr($game->get('field_date')->value, 5);
      if ($game_date == $today) {
        $render_controller = \Drupal::entityTypeManager()->getViewBuilder($game->getEntityTypeId());
        $build['#games'][] = $render_controller->view($game, 'card');
      }
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Don't cache this block, otherwise it shows the wrong date.
    return 0;
  }
}
