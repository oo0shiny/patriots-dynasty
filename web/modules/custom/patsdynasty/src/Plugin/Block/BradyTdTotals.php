<?php

namespace Drupal\patsdynasty\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "brady_total_tds",
 *   admin_label = @Translation("Brady Total TDs"),
 *   category = @Translation("Custom"),
 * )
 */
class BradyTdTotals extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $player_tds = [];
    $all_tds = [];
    $players = $this->get_players();
    $teams = $this->get_teams();
    // Get all Gif paragraphs tagged with Brady + Pass + TD.
    $pids = \Drupal::entityQuery('paragraph')
      ->condition('type', 'gif')
      ->condition('field_players_involved', '399')
      ->condition('field_td_scored', TRUE)
      ->condition('field_play_type', '54')
      ->sort('field_season' , 'DESC')
      ->execute();
    $pp_storage = \Drupal::entityTypeManager()->getStorage('paragraph');
    $gif_pps = $pp_storage->loadMultiple($pids);
    // TDs sorted by player.
    foreach ($gif_pps as $gif) {
      $players_involved = $gif->get('field_players_involved')->getValue();
      // Only count plays where Brady was the one who threw the TD.
      if ($players_involved[count($players_involved) - 2]['target_id'] == '399') {
        $receiver = array_pop($players_involved);
        $name = $players[$receiver['target_id']]['name'];
        $position = $players[$receiver['target_id']]['position'];
        if ($gif->get('field_week')->value < 18) {
          $week = 'Week ' . $gif->get('field_week')->value;
        }
        else {
          switch ($gif->get('field_week')->value) {
            case 18:
              $week = 'Wildcard';
              break;
            case 19:
              $week = 'AFC Divisional Round';
              break;
            case 20:
              $week = 'AFC Conference Championship';
              break;
            case 21:
              $week = 'Super Bowl';
              break;
          }
        }
        $player_tds[$name][] = [
          'title' => $gif->get('field_gif_title')->value,
          'link' => 'https://gfycat.com/' . $gif->get('field_gfycat_embed_code')->value,
          'season' => $gif->get('field_season')->value,
          'week' => $week,
          'opp' => $teams[$gif->get('field_opponent')->target_id],
        ];

        $position_tds[$position][] = [
          'title' => $gif->get('field_gif_title')->value,
          'link' => 'https://gfycat.com/' . $gif->get('field_gfycat_embed_code')->value,
          'season' => $gif->get('field_season')->value,
          'week' => $week,
          'opp' => $teams[$gif->get('field_opponent')->target_id],
        ];
        $all_tds[$gif->get('field_season')->value][$week][] = [
          'title' => $gif->get('field_gif_title')->value,
          'link' => 'https://gfycat.com/' . $gif->get('field_gfycat_embed_code')->value,
          'opp' => $teams[$gif->get('field_opponent')->target_id],
          'week' => $week,
        ];
      }
    }
    array_multisort(array_map('count', $player_tds), SORT_DESC, $player_tds);
    array_multisort(array_map('count', $position_tds), SORT_DESC, $position_tds);
    $ordered_games = [];
    foreach ($all_tds as $season => $weeks) {
      krsort($weeks, SORT_NATURAL);
      $ordered_games[$season] = $weeks;
    }
    $metadata = [];
    foreach ($player_tds as $player => $gifs) {
      $metadata[] = [
        'player' => $player,
        'count' => count($gifs)
      ];
    }

    // TDs sorted by quarter.
    $quarter_tds = [
      '1' => [],
      '2' => [],
      '3' => [],
      '4' => [],
      'OT' => []
    ];
    foreach ($gif_pps as $gif) {
      if (!$gif->get('field_quarter')->isEmpty()) {
        $quarter = $gif->get('field_quarter')->value;
        if ($quarter == 5) {
          $quarter = 'OT';
        }
        $quarter_tds[$quarter][] = [
          'title' => $gif->get('field_gif_title')->value,
          'link' => 'https://gfycat.com/' . $gif->get('field_gfycat_embed_code')->value,
          'season' => $gif->get('field_season')->value,
          'opp' => $teams[$gif->get('field_opponent')->target_id],
        ];
      }
    }
    $q_count = [];
    foreach ($quarter_tds as $quarter => $gifs) {
      $q_count[$quarter] = count($gifs);
    }
    $quarter_tds['1st Quarter'] = $quarter_tds[1];
    $quarter_tds['2nd Quarter'] = $quarter_tds[2];
    $quarter_tds['3rd Quarter'] = $quarter_tds[3];
    $quarter_tds['4th Quarter'] = $quarter_tds[4];
    $quarter_tds['Overtime'] = $quarter_tds['OT'];
    unset($quarter_tds[1]);
    unset($quarter_tds[2]);
    unset($quarter_tds[3]);
    unset($quarter_tds[4]);
    unset($quarter_tds['OT']);

    // TDs sorted by distance.
    $yards_tds = [];
    foreach ($gif_pps as $gif) {
      if (!$gif->get('field_yards_gained')->isEmpty()) {
        $yards = $gif->get('field_yards_gained')->value;
        $yards_tds[$yards][] = [
          'title' => $gif->get('field_gif_title')->value,
          'link' => 'https://gfycat.com/' . $gif->get('field_gfycat_embed_code')->value,
          'season' => $gif->get('field_season')->value,
          'opp' => $teams[$gif->get('field_opponent')->target_id],
        ];
      }
    }
    krsort($yards_tds);



    return [
      '#theme' => 'brady_total_tds',
      '#players' => $player_tds,
      '#playercount' => $metadata,
      '#quarters' => $quarter_tds,
      '#q_count' => $q_count,
      '#tdyards' => $yards_tds,
      '#tdposition' => $position_tds,
      '#alltds' => $ordered_games
    ];
  }

  /**
   * Helper to get array of players.
   */
  function get_players() {
    $players = [];
    $nids = \Drupal::entityQuery('node')->condition('type','player')->execute();
    $nodes =  Node::loadMultiple($nids);
    $positions = $this->get_positions();
    foreach ($nodes as $player) {
      $pos = $player->get('field_player_position')->target_id;
      $players[$player->id()] = [
        'name' => $player->getTitle(),
        'position' => $positions[$pos],
      ];
    }
    return $players;
  }

  function get_teams() {
    $teams = [];
    $nids = \Drupal::entityQuery('node')->condition('type','team')->execute();
    $nodes =  Node::loadMultiple($nids);

    foreach ($nodes as $team) {
      $title = explode(' ', $team->getTitle());
      $teams[$team->id()] = array_pop($title);
    }
    return $teams;
  }

  function get_positions() {
    $positions = [];
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('position');
    foreach ($terms as $term) {
      $positions[$term->tid] = $term->name;
    }
    return $positions;
  }

}
