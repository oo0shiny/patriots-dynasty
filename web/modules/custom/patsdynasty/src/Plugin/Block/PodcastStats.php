<?php

namespace Drupal\patsdynasty\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
/**
 * Provides a Block that displays games from this date in history.
 *
 * @Block(
 *   id = "podcast_stats",
 *   admin_label = @Translation("Podcast Stats Block"),
 *   category = @Translation("Custom"),
 * )
 */
class PodcastStats extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    // Get all games and see if the day/month matches up to today.
    $pod_nodes = \Drupal::entityQuery('node')
      ->condition('type', 'podcast_episode')
      ->execute();

    $episodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($pod_nodes);


    $total = 0;
    $count = 0;
    $first30 = 0;
    foreach ($episodes as $episode) {
      $total += $episode->get('field_total_downloads')->value;
      $first30 += $episode->get('field_first_30_days')->value;
      $count ++;
    }

    return [
      '#theme' => 'podcast_stats',
      '#total' => $total,
      '#episodes' => $count,
      '#first30' => $first30
    ];
  }
}
