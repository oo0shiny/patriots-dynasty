<?php
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;

/**
 * Implements hook_views_query_alter().
 *
 * @param \Drupal\views\ViewExecutable $view
 * @param \Drupal\views\Plugin\views\query\QueryPluginBase $query
 */
function patsdynasty_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() == 'date_based_breakdowns' ) {
    $query->fields['node__field_date_field_date_value_month']['field'] = "DATE_FORMAT(node__field_date.field_date_value, '%m')";
  }
}
